import subprocess
import re
import argparse


def get_commits(mode, count=None, since_date=None):
    """Retrieve commit hashes based on the selected mode."""
    if mode == "count":
        cmd = ["git", "log", f"-n {count}", "--pretty=format:%H"]
    elif mode == "since":
        cmd = ["git", "log", f"--since={since_date}", "--pretty=format:%H"]
    else:
        raise ValueError("Unknown mode: use 'count' or 'since'.")

    result = subprocess.run(cmd, stdout=subprocess.PIPE, text=True)
    return result.stdout.splitlines()


def get_commit_messages(commit):
    """Retrieve the commit message for a given commit."""
    cmd = ["git", "log", "-1", "--pretty=format:%s", commit]
    result = subprocess.run(cmd, stdout=subprocess.PIPE, text=True)
    return result.stdout


def get_modified_files(commit):
    """List the files modified in a given commit."""
    cmd = ["git", "diff-tree", "--no-commit-id", "--name-only", "-r", commit]
    result = subprocess.run(cmd, stdout=subprocess.PIPE, text=True)
    return result.stdout.splitlines()


def check_keywords_in_content(content, keywords):
    """Check if content contains any of the specified keywords."""
    return any(re.search(keyword, content, re.IGNORECASE) for keyword in keywords)


def main():
    # Define CLI arguments
    parser = argparse.ArgumentParser(description="Search for keywords in Git commits.")
    parser.add_argument("--count", type=int, help="Number of commits to check.")
    parser.add_argument("--since", type=str,
                        help="Start date for commit search (e.g., '2023-01-01').")
    parser.add_argument("--keywords", nargs="+", required=True,
                        help="List of keywords to search for.")
    parser.add_argument("--mode", type=str, choices=["file", "commit", "all"], default="file",
                        help="Mode of search: 'file' (default), 'commit', or 'all'.")
    args = parser.parse_args()

    # Determine the mode for fetching commits
    if args.count:
        fetch_mode = "count"
    elif args.since:
        fetch_mode = "since"
    else:
        raise ValueError("You must specify either --count or --since.")

    # Get the list of commits
    commits = get_commits(fetch_mode, count=args.count, since_date=args.since)

    # Iterate through commits and apply the selected mode
    for commit in commits:
        # print(f"Checking commit: {commit}")

        if args.mode in ["commit", "all"]:
            # Check commit message
            commit_message = get_commit_messages(commit)
            if check_keywords_in_content(commit_message, args.keywords):
                print(f"Keyword match in commit message: {commit}")

        if args.mode in ["file", "all"]:
            # Check modified files
            modified_files = get_modified_files(commit)
            for file in modified_files:
                cmd = ["git", "show", f"{commit}:{file}"]
                try:
                    result = subprocess.run(cmd, stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE, text=True)
                    file_content = result.stdout
                    if check_keywords_in_content(file_content, args.keywords):
                        print(f"Keyword match in commit {commit}, file: {file}")
                except subprocess.CalledProcessError:
                    # Skip files that cannot be accessed
                    continue


if __name__ == "__main__":
    main()
