#!/usr/bin/env python3
import pymysql
import argparse
from wmflib.interactive import ask_confirmation

# Default MySQL configuration file
MY_CNF_PATH = "/etc/my.cnf"
NOBINLOG = "set session sql_log_bin=0;"

def connect_to_db(host=None):
    """
    Connects to the database using the default configuration from /etc/my.cnf,
    or to the specified host if provided.
    """
    try:
        if host:
            connection = pymysql.connect(
                host=host,
                read_default_file=MY_CNF_PATH,
                autocommit=True
            )
        else:
            connection = pymysql.connect(
                read_default_file=MY_CNF_PATH,
                autocommit=True
            )
        return connection
    except pymysql.MySQLError as e:
        print(f"Error connecting to the database: {e}")
        exit(1)

def get_replication_info(source_host):
    """
    Connects to the source replication server and fetches the necessary
    binlog file and position for replication.
    """
    connection = connect_to_db(source_host)
    try:
        with connection.cursor() as cursor:
            cursor.execute("SHOW MASTER STATUS;")
            result = cursor.fetchone()
            if not result:
                raise RuntimeError("Failed to fetch master status from the source.")
            master_log_file, master_log_pos = result[0], result[1]
            print(f"Fetched replication info: Log File = {master_log_file}, Position = {master_log_pos}")
            return master_log_file, master_log_pos
    finally:
        connection.close()

def list_tables(cursor):
    """
    Retrieves all tables from the parsercache database.
    """
    cursor.execute("USE parsercache;")
    cursor.execute("SHOW TABLES;")
    return [row[0] for row in cursor.fetchall()]

def truncate_tables(cursor, tables):
    """
    Truncates all specified tables in the database.

    Temporarily disables binary logging during the truncation.
    """
    if not ask_confirmation("Are you sure you want to truncate all tables in the parsercache database?"):
        print("Operation canceled by the user.")
        return False

    for table in tables:
        print(f"Truncating table: {table}")
        cursor.execute(f"{NOBINLOG} TRUNCATE TABLE `{table}`;")
    return True

def configure_replication(cursor, replication_source):
    """
    Configures replication with a new source.

    Stops the current replication, sets the new source, and restarts replication.
    """
    if not ask_confirmation("Are you sure you want to change the replication source?"):
        print("Operation canceled by the user.")
        return False

    print("Stopping current replication...")
    cursor.execute("STOP SLAVE;")
    cursor.execute("RESET SLAVE;")
    print("Configuring new replication source...")
    cursor.execute(f"""
        CHANGE MASTER TO
            MASTER_HOST = '{replication_source['host']}',
            MASTER_PORT = {replication_source['port']},
            MASTER_LOG_FILE = '{replication_source['log_file']}',
            MASTER_LOG_POS = {replication_source['log_pos']},
            MASTER_SSL = 1;
    """)
    print("Starting replication...")
    cursor.execute("START SLAVE;")
    return True


def check_and_create_parsercache(cursor):
    """
    Checks if parsercache database and tables exist, creates them if they don't.
    Returns True if everything is ready, False if creation was needed.
    """
    # Check if database exists
    cursor.execute("SHOW DATABASES LIKE 'parsercache';")
    db_exists = cursor.fetchone() is not None

    if not db_exists:
        if not ask_confirmation("parsercache database doesn't exist. Would you like to create it?"):
            print("Error: parsercache database is required but doesn't exist.")
            return False

        print("Creating parsercache database...")
        cursor.execute(f"{NOBINLOG} CREATE DATABASE parsercache;")

    cursor.execute("USE parsercache;")

    # Get existing tables
    cursor.execute("SHOW TABLES;")
    existing_tables = {row[0] for row in cursor.fetchall()}

    # Define table structure
    table_structure = """
    CREATE TABLE IF NOT EXISTS `{}` (
        keyname varbinary(255) NOT NULL DEFAULT '',
        value mediumblob DEFAULT NULL,
        exptime datetime DEFAULT NULL,
        UNIQUE KEY keyname (keyname),
        KEY exptime (exptime)
    ) ENGINE=InnoDB DEFAULT CHARSET=binary;
    """
    table_structure = NOBINLOG + table_structure

    # Check if all required tables exist
    missing_tables = []
    for i in range(256):
        table_name = f"pc{i:03d}"
        if table_name not in existing_tables:
            missing_tables.append(table_name)

    if missing_tables:
        if not ask_confirmation(f"{len(missing_tables)} tables are missing. Would you like to create them?"):
            print("Error: some required tables are missing.")
            return False

        print("Creating missing tables...")
        for table_name in missing_tables:
            print(f"Creating table {table_name}...")
            cursor.execute(table_structure.format(table_name))

    return True

def main():
    """
    Main function that connects to the database, truncates tables,
    and configures replication with user confirmation steps.
    """
    parser = argparse.ArgumentParser(description="Manage replication and clean tables for parsercache.")
    parser.add_argument("--repl-source", required=True, help="Replication source hostname (e.g., pc1015.eqiad.wmnet).")
    args = parser.parse_args()

    # Step 1: Fetch replication information from the source server
    print(f"Connecting to replication source: {args.repl_source}")
    master_log_file, master_log_pos = get_replication_info(args.repl_source)

    replication_source = {
        "host": args.repl_source,
        "port": 3306,
        "log_file": master_log_file,
        "log_pos": master_log_pos,
    }

    # Step 2: Connect to the local database
    connection = connect_to_db()
    try:
        with connection.cursor() as cursor:
            # Step 3: Check and create database/tables if needed
            if not check_and_create_parsercache(cursor):
                print("Exiting: parsercache database or tables are not ready.")
                return

            # Step 4: List tables
            tables = list_tables(cursor)
            print(f"Tables found in 'parsercache': {tables}")

            # Step 5: Truncate all tables
            if truncate_tables(cursor, tables):
                print("All tables were successfully truncated.")

            # Step 6: Configure replication
            if configure_replication(cursor, replication_source):
                print("Replication was successfully configured.")
    finally:
        connection.close()

if __name__ == "__main__":
    main()
