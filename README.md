# Database Switchover and Replication Tools

This repository contains a set of Python scripts designed to manage database switchover and replication processes for MariaDB databases.

## Tools

### `pc-switch.py`

WIP

### `init-repl.py`

This script initializes MariaDB replication between two servers. It performs the following actions:

1. Connects to the source server and retrieves its binary log file and position.
2. Connects to the replica server and configures it for replication.
3. Starts the replication process on the replica server.
4. Verifies the replication status to ensure it is working correctly.

### `misc-switchover.py`

This script manages the [misc](https://wikitech.wikimedia.org/wiki/MariaDB#Misc_section_failover_checklist_(example_with_m2) database switchover process. It performs the following actions:

1. Checks configuration differences between the new and old source hosts.
2. Performs the switchover from the old source to the new source.
3. Updates the configuration to set the new source as the primary host for the section.

Example run:
```
./misc-switchover.py --dry-run --new-source db1234 --old-source db1123 --section m5
INFO:__main__:[DRY RUN] Would execute: cumin 'db1234*' 'puppet agent --disable "switchover to db1234"'
INFO:__main__:[DRY RUN] Would execute: cumin 'db1123*' 'puppet agent --disable "switchover to db1234"'
INFO:__main__:Checking configuration differences between new and old source.
INFO:__main__:Executing: pt-config-diff h=db1123,F=/root/.my.cnf h=db1234,F=/root/.my.cnf
INFO:__main__:[DRY RUN] Would execute: pt-config-diff h=db1123,F=/root/.my.cnf h=db1234,F=/root/.my.cnf
INFO:__main__:Starting failover from db1123 to db1234.
INFO:__main__:Executing: db-switchover --timeout=15 --only-slave-move db1123 db1234
INFO:__main__:[DRY RUN] Would execute: db-switchover --timeout=15 --only-slave-move db1123 db1234
INFO:__main__:Updating configuration to set db1234 as the new source for section m5.
INFO:__main__:Executing: dbctl --scope eqiad section m5 set-master db1234
INFO:__main__:[DRY RUN] Would execute: dbctl --scope eqiad section m5 set-master db1234
INFO:__main__:Executing: dbctl config commit -b -m 'Promote db1234 to m5 source'
INFO:__main__:[DRY RUN] Would execute: dbctl config commit -b -m 'Promote db1234 to m5 source'
INFO:__main__:[DRY RUN] Would execute: cumin 'db1123*' 'pt-kill --print --kill --victims all --match-all F=/dev/null,S=/run/mysqld/mysql.sock'
INFO:__main__:[DRY RUN] Would execute: cumin 'db1234*' 'run-puppet-agent -f'
INFO:__main__:[DRY RUN] Would execute: cumin 'db1123*' 'run-puppet-agent -f'
INFO:__main__:[DRY RUN] Would execute: db-mysql db1234 -e 'select * from heartbeat.heartbeat where file like "%db1234%";'
INFO:__main__:[DRY RUN] Would execute: db-mysql db1234 -e 'delete from heartbeat.heartbeat where file like "%db1234%";'
INFO:__main__:[DRY RUN] Would execute: curl -sS 'https://gerrit.wikimedia.org/r/plugins/gitiles/operations/software/+/refs/heads/master/dbtools/events_coredb_master.sql?format=TEXT' | base64 -d | sudo db-mysql db1234
INFO:__main__:[DRY RUN] Would execute: curl -sS 'https://gerrit.wikimedia.org/r/plugins/gitiles/operations/software/+/refs/heads/master/dbtools/events_coredb_slave.sql?format=TEXT' | base64 -d | sudo db-mysql db1123
INFO:__main__:Failover completed successfully.
INFO:__main__:[DRY RUN] Would execute: cumin 'db1234*' 'puppet agent --enable'
INFO:__main__:[DRY RUN] Would execute: cumin 'db1123*' 'puppet agent --enable'
```

### pc-bootstrap

This script has to be run on the server you're managing. It manages the `parsercache` database by:
1. Listing and truncating all tables in the `parsercache` database.
2. Configuring the local server as a replica of a specified replication source.

## Usage

To use these scripts, simply run them with the required arguments. For example:

* `python pc-switch.py`
* `python init-repl.py --source <source_server> --replica <replica_server>`
* `python misc-switchover.py --new-source <new_source> --old-source <old_source> --section <section>`
* `python3 pc-bootstrap.py --repl-source pc1015.eqiad.wmnet`

Note: These scripts assume that the replication credentials are stored in a YAML file at `/etc/spicerack/cookbooks/sre.switchdc.databases.yaml`.

### `irc-log-query`

* `./irc-log-query.py --channel wikimedia-operations --keyword "#page" --since 20241101 --parallelize 30 --keyword MariaDB`

[![asciicast](https://asciinema.org/a/wstx1rsOTdSQaJjwRnFR6M0K2.svg)](https://asciinema.org/a/wstx1rsOTdSQaJjwRnFR6M0K2)

```
usage: irc-log-query.py [-h] [--channel CHANNEL] [--context CONTEXT] [--keyword KEYWORD] [--exclude EXCLUDE] [--parallelize PARALLELIZE] [--debug] [--inclusive] [--from-user FROM_USER]
                        [--extract] [--since SINCE | --from-date FROM_DATE] [--to TO]

Download and search through Wikimedia Libera logs.

options:
  -h, --help            show this help message and exit
  --channel CHANNEL     The channel to download logs from (default: wikimedia-data-persistence)
  --context CONTEXT     Number of context lines to display around matches (default: 2)
  --keyword KEYWORD     Keyword to search for in the logs (can be specified multiple times)
  --exclude EXCLUDE     Exclude lines containing this keyword (case insensitive)
  --parallelize PARALLELIZE
                        Number of parallel downloads (default: 1)
  --debug               Enable debug mode to print additional information
  --inclusive           Enable inclusive search mode to match any keyword (default is all keywords must match)
  --from-user FROM_USER
                        Username to search for in the logs, only lines from this user will be included (can be specified multiple times)
  --extract             Extract matching lines to a log file instead of using fzf for interactive filtering
  --since SINCE         The start date for logs to download (format: YYYYMMDD)
  --from-date FROM_DATE
                        The start date for logs to download (format: YYYYMMDD)
  --to TO               The end date for logs to download (format: YYYYMMDD), required if using --from-dat
```
