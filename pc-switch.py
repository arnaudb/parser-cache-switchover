#!/usr/bin/env python3
import subprocess
import json
import re
from helpers.runner import run_command

def get_hosts():
    """Get all hosts starting with 'pc', can become parametric"""
    output = run_command("dbctl section all get")
    data = json.loads(output)
    hosts = []
    for item in data:
        if item['name'].startswith('pc'):
            hosts.append(item['name'])
    return hosts, output

def select_host(hosts):
    """Select a host from the list"""
    print("Select a host to be replaced:")
    for i, host in enumerate(hosts):
        print(f"{i + 1}. {host}")
    index = int(input("Enter the number of the host: ")) - 1
    return hosts[index]

def get_section(host, output):
    """Get the section of the selected host"""
    data = json.loads(output)
    for item in data:
        if item.get('master') == host:
            return item['section']
    raise Exception(f"Section not found for host {host}")


def get_scope(host):
    """Get the scope of the host based on its name"""
    match = re.search(r'\D{2}(\d+)', host)
    if not match:
        raise Exception(f"Unable to determine scope for host {host}")
    num = match.group(1)
    if num.startswith('1'):
        return 'eqiad'
    elif num.startswith('2'):
        return 'codfw'
    # Add more conditions here for other prefixes if necessary
    else:
        raise Exception(f"Unknown scope prefix for host {host}")

def get_spare_host(section, old_host):
    """Get a spare host for the given section"""
    scope = get_scope(old_host)
    output = run_command(f"dbctl --scope {scope} instance all get")
    data = json.loads(output)
    spares = [item['name'] for item in data if section in item.get(
        'sections', {}) and 'spare' in item.get('note', '')]
    if not spares:
        raise Exception(f"No spare host found for section {section}")
    return spares[0], scope

def disable_replication(spare_host):
    """Disable replication on the spare host"""
    command = f"db-mysql {spare_host} -e 'stop slave; reset slave all'"
    run_command(command)


def replace_host(section, old_host, new_host, scope):
    """Replace the old host with the new host in the given section"""
    commands = [
        f"dbctl instance {new_host} pool --section {section}",
        f"dbctl --scope {scope} section {section} set-master {new_host}",
        f"dbctl instance {old_host} depool",
        f"dbctl config commit -m \"Replace {
            old_host} with {new_host} for {section}\""
    ]
    for command in commands:
        run_command(command)

def main():
    # Step 1: Get and select the host to be replaced
    hosts, output = get_hosts()
    old_host = select_host(hosts)
    print(f"Selected host: {old_host}")

    # Step 2: Identify the section of the selected host
    section = get_section(old_host, output)
    print(f"Section: {section}")

    # Step 3: Identify a spare host for the section
    spare_host, scope = get_spare_host(section, old_host)
    print(f"Spare host: {spare_host}")

    # Step 4: Confirm the replacement
    confirm = input(f"Do you want to replace {old_host} with {spare_host} in section {
                    section}? Type the shortname of the spare host to confirm: ")
    if confirm != spare_host.split('.')[0]:
        print("Confirmation failed. Operation cancelled.")
        return

    # Step 5: Disable replication on the spare host
    disable_replication(spare_host)

    # Step 6: Replace the old host with the spare host
    replace_host(section, old_host, spare_host, scope)

    print(f"Successfully replaced {old_host} with {
          spare_host} in section {section}.")

if __name__ == "__main__":
    main()
