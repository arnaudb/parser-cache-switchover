#!/usr/bin/env python3
import os
import re
import requests
import argparse
import shutil
import tempfile
from datetime import datetime, timedelta
from dateutil.parser import parse as dateparse
import subprocess
import concurrent.futures
import chardet
import codecs

# Function to download a file
def download_file(url, dest, debug=False):
    if debug:
        print(f"Attempting to download: {url}")
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        try:
            with open(dest, 'wb') as f:
                for chunk in response.iter_content(chunk_size=8192):
                    if chunk:  # Filter keep-alive chunks
                        f.write(chunk)
            if debug:
                print(f"Downloaded: {url}")
        except Exception as e:
            print(f"Failed to write file {dest}: {e}")
    else:
        print(f"Failed to download: {url} with status code {response.status_code}")

# Main function
def main():
    parser = argparse.ArgumentParser(
        description="Download and search through Wikimedia Libera logs.")
    parser.add_argument('--channel', type=str, default='wikimedia-data-persistence',
                        help='The channel to download logs from (default: wikimedia-data-persistence)')
    parser.add_argument('--context', type=int, default=2,
                        help='Number of context lines to display around matches (default: 2)')
    parser.add_argument('--keyword', type=str, action='append', required=False,
                        help='Keyword to search for in the logs (can be specified multiple times)')
    parser.add_argument('--exclude', type=str,
                        help='Exclude lines containing this keyword (case insensitive)')
    parser.add_argument('--parallelize', type=int, default=1,
                        help='Number of parallel downloads (default: 1)')
    parser.add_argument('--debug', action='store_true',
                        help='Enable debug mode to print additional information')
    parser.add_argument('--inclusive', action='store_true',
                        help='Enable inclusive search mode to match any keyword (default is all keywords must match)')
    parser.add_argument('--from-user', type=str, action='append',
                        help='Username to search for in the logs, only lines from this user will be included (can be specified multiple times)')
    parser.add_argument('--extract', action='store_true',
                        help='Extract matching lines to a log file instead of using fzf for interactive filtering')

    # Mutually exclusive group for date options
    date_group = parser.add_mutually_exclusive_group()
    date_group.add_argument('--since', type=str,
                            help='The start date for logs to download (format: YYYYMMDD)')
    date_group.add_argument('--from-date', type=str,
                            help='The start date for logs to download (format: YYYYMMDD)')
    parser.add_argument('--to', type=str,
                        help='The end date for logs to download (format: YYYYMMDD), required if using --from-date')

    args = parser.parse_args()

    # If --from-user is used without --keyword, set --keyword to the username(s)
    if args.from_user and not args.keyword:
        args.keyword = args.from_user

    if args.from_date and not args.to:
        print("Error: --to must be specified if using --from-date")
        return

    base_url = "https://wm-bot.wmflabs.org/libera_logs/%23"
    channel_url = f"{base_url}{args.channel}/"

    # Create a temporary directory to store the downloaded files
    temp_dir = tempfile.mkdtemp()

    if args.debug:
        print(f"Temporary directory created at: {temp_dir}")

    # Determine the download period
    if args.since:
        since_date = dateparse(args.since)
        end_date = datetime.now()
    elif args.from_date:
        since_date = dateparse(args.from_date)
        end_date = dateparse(args.to)
    else:
        since_date = datetime.now() - timedelta(days=7)
        end_date = datetime.now()

    if args.debug:
        print(f"Starting download from date: {since_date.strftime(
            '%Y-%m-%d')} to {end_date.strftime('%Y-%m-%d')}")

    # Download the necessary files in parallel if specified
    current_date = since_date
    dates_to_download = []
    while current_date <= end_date:
        date_str = current_date.strftime('%Y%m%d')
        file_url = f"{channel_url}{date_str}.txt"
        dest_file = os.path.join(temp_dir, f"{date_str}.txt")
        dates_to_download.append((file_url, dest_file))
        current_date += timedelta(days=1)

    with concurrent.futures.ThreadPoolExecutor(max_workers=args.parallelize) as executor:
        futures = [executor.submit(download_file, url, dest, args.debug)
                   for url, dest in dates_to_download]
        concurrent.futures.wait(futures)

    # Search for keywords and optionally usernames in the files
    matching_lines = []
    for root, _, files in os.walk(temp_dir):
        for file in files:
            if file.endswith(".txt"):
                try:
                    file_path = os.path.join(root, file)
                    if args.debug:
                        print(f"Reading file: {file_path}")

                    # Detect the file encoding
                    with open(file_path, 'rb') as f:
                        # Read a portion of the file to detect encoding
                        raw_data = f.read(1024)
                        result = chardet.detect(raw_data)
                        encoding = result['encoding']

                    # Fallback if encoding is not detected or is None
                    if not encoding:
                        encoding = 'utf-8'

                    # Read the file with the detected encoding
                    with codecs.open(file_path, 'r', encoding=encoding, errors='replace') as f:
                        lines = f.read().splitlines()  # Use splitlines to avoid newline issues
                        if args.debug:
                            print(f"Content of {file_path}:{lines}")
                        for i, line in enumerate(lines):
                            if args.inclusive:
                                # Match if any keyword is present
                                keyword_match = any(re.search(keyword, line, re.IGNORECASE) for keyword in args.keyword)
                            else:
                                # Match only if all keywords are present
                                keyword_match = all(re.search(keyword, line, re.IGNORECASE) for keyword in args.keyword)

                            if keyword_match:
                                # Check if line is from specified user(s) if provided
                                if args.from_user:
                                    user_match = any(
                                        re.search(f"<{user}>", line) for user in args.from_user)
                                    if not user_match:
                                        continue

                                if args.exclude and re.search(args.exclude, line, re.IGNORECASE):
                                    if args.debug:
                                        print(f"Excluding line: {line}")
                                    continue
                                start = max(i - args.context, 0)
                                end = min(i + args.context + 1, len(lines))
                                for j in range(start, end):
                                    matching_lines.append(f"{file}: {lines[j]}")
                                # Add a separator between results
                                matching_lines.append("\n---\n")
                except UnicodeDecodeError as e:
                    print(f"Failed to read file {file} due to encoding error: {e}")

    # Handle matching lines based on the --extract option
    if matching_lines:
        if args.debug:
            print(f"Number of matching lines found: {len(matching_lines)}")
        if args.extract:
            # Write matching lines to a log file
            output_filename = f"extracted_results_{
                datetime.now().strftime('%Y%m%d_%H%M%S')}.log"
            with open(output_filename, 'w', encoding='utf-8') as output_file:
                output_file.write('\n'.join(matching_lines))
            if args.debug:
                print(f"Extracted results written to {output_filename}")
            else:
                print(f"Extracted results saved to {output_filename}")
        else:
            # Pass matching lines to fzf for interactive filtering
            try:
                result = subprocess.run(['fzf'], input='\n'.join(
                    matching_lines), text=True, capture_output=True)
                if args.debug:
                    print("\nFiltered results:\n"+'\n'.join(matching_lines))
                print(result.stdout)
            except FileNotFoundError:
                print("fzf is not installed. Please install fzf to use this feature.")
    else:
        print("No matches found.")

    # Clean up the temporary directory
    shutil.rmtree(temp_dir)
    if args.debug:
        print("Temporary files cleaned up.")


if __name__ == "__main__":
    main()
