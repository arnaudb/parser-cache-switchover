#!/usr/bin/env python3
import argparse
import logging
from wmflib.interactive import ask_confirmation
from helpers.runner import run_command

# Basic logger configuration
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def remote_exec(host, command, dry_run=False):
    """Exécute une commande sur le serveur donné."""
    # Trim the FQDN to get the prefix
    host_prefix = host.split('.')[0]

    # Create the cumin command
    cumin_command = f"cumin '{host_prefix}*' '{command}'"

    # Ask for confirmation
    if dry_run or ask_confirmation(f"Execute command: {cumin_command}?"):
        if dry_run:
            logger.info(f"[DRY RUN] Would execute: {cumin_command}")
            return
        # Run the command
        logger.info(f"Executing: {cumin_command}")
        output = run_command(cumin_command)
        logger.info(f"Command output: {output}")

def local_exec(command, dry_run=False):
    """Exécute une commande locale."""
    # Ask for confirmation
    if dry_run or ask_confirmation(f"Execute command: {command}?"):
        if dry_run:
            logger.info(f"[DRY RUN] Would execute: {command}")
            return
        # Run the command
        logger.info(f"Executing: {command}")
        output = run_command(command)
        logger.info(f"Command output: {output}")

VALID_SECTIONS = ('m1', 'm2', 'm3', 'm5')

def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="Manage MariaDB failover process.")
    parser.add_argument(
        "--new-source",
        required=True,
        help="Hostname of the new source"
    )
    parser.add_argument(
        "--old-source",
        required=True,
        help="Hostname of the old source"
    )
    parser.add_argument(
        "--section",
        required=True,
        choices=VALID_SECTIONS,
        help=f"Database section (valid choices: {', '.join(VALID_SECTIONS)})"
    )
    parser.add_argument(
        "--dbproxies",
        nargs="*",
        help="List of dbproxies to reload (optional)"
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Enable dry-run mode - commands will be logged but not executed"
    )
    return parser.parse_args()

def pick_dc(server_name):
    """Determine the scope based on the server name."""
    return "eqiad" if server_name.startswith("db1") else "codfw"

def check_configuration(new_source, old_source, dry_run=False):
    logger.info("Checking configuration differences between new and old source.")
    pt_config_diff_command = f"pt-config-diff h={old_source},F=/root/.my.cnf h={new_source},F=/root/.my.cnf"
    logger.info(f"Executing: {pt_config_diff_command}")
    if dry_run or ask_confirmation(f"Execute command: {pt_config_diff_command}?"):
        if dry_run:
            logger.info(f"[DRY RUN] Would execute: {pt_config_diff_command}")
            return
        output = run_command(pt_config_diff_command)
        if not dry_run:
            ask_confirmation(f"Diff output: {output}")

def switch_source(new_source, old_source, dry_run=False):
    logger.info(f"Starting failover from {old_source} to {new_source}.")
    db_switchover_command = f"db-switchover --timeout=15 --only-slave-move {old_source} {new_source}"
    logger.info(f"Executing: {db_switchover_command}")
    if dry_run or ask_confirmation(f"Execute command: {db_switchover_command}?"):
        if dry_run:
            logger.info(f"[DRY RUN] Would execute: {db_switchover_command}")
            return
        output = run_command(db_switchover_command)
        logger.info(f"Switchover output: {output}")

def update_configuration(new_source, section, dc, dry_run=False):
    logger.info(f"Updating configuration to set {new_source} as the new source for section {section}.")
    dbctl_command = f"dbctl --scope {dc} section {section} set-master {new_source}"
    logger.info(f"Executing: {dbctl_command}")
    if dry_run or ask_confirmation(f"Execute command: {dbctl_command}?"):
        if dry_run:
            logger.info(f"[DRY RUN] Would execute: {dbctl_command}")
        else:
            output = run_command(dbctl_command)
            logger.info(f"Config update output: {output}")

    dbctl_command = f"dbctl config commit -b -m 'Promote {new_source} to {section} source'"
    logger.info(f"Executing: {dbctl_command}")
    if dry_run or ask_confirmation(f"Execute command: {dbctl_command}?"):
        if dry_run:
            logger.info(f"[DRY RUN] Would execute: {dbctl_command}")
        else:
            output = run_command(dbctl_command)
            logger.info(f"Config update output: {output}")

def main():
    # Parse command line arguments
    args = parse_arguments()
    new_source = args.new_source
    old_source = args.old_source
    section = args.section
    dbproxies = args.dbproxies
    dry_run = args.dry_run

    # Disable puppet
    remote_exec(new_source, 'puppet agent --disable "switchover to ' + new_source + '"', dry_run)
    remote_exec(old_source, 'puppet agent --disable "switchover to ' + new_source + '"', dry_run)

    # Check if servers are in the same DC
    new_dc = "eqiad" if new_source.startswith("db1") else "codfw"
    old_dc = "eqiad" if old_source.startswith("db1") else "codfw"

    if new_dc != old_dc:
        logger.error("Cross-DC switchover is not supported")
        return

    # Determine the scope based on the new source hostname
    dc = pick_dc(new_source)

    # Confirm the new source with the user
    if not dry_run and not ask_confirmation(f"Is '{new_source}' the correct hostname for the new source?"):
        logger.info("Failover aborted by the user.")
        return

    # Check configuration differences
    check_configuration(new_source, old_source, dry_run)

    # Ask for confirmation to proceed with the switchover
    if dry_run or ask_confirmation("Do you want to proceed with the source switchover?"):
        # Perform the switchover
        switch_source(new_source, old_source, dry_run)

        # Update the configuration
        update_configuration(new_source, section, dc, dry_run)

        # Reload haproxies if specified
        if dbproxies:
            for dbproxy in dbproxies:
                remote_exec(dbproxy, 'systemctl reload haproxy && echo "show stat" | socat /run/haproxy/haproxy.sock stdio', dry_run)

        # Kill connections on old master
        remote_exec(old_source, 'pt-kill --print --kill --victims all --match-all F=/dev/null,S=/run/mysqld/mysql.sock', dry_run)

        # Restart puppet on old and new masters
        remote_exec(new_source, 'run-puppet-agent -f', dry_run)
        remote_exec(old_source, 'run-puppet-agent -f', dry_run)

        # Clean up lag in orchestrator
        local_exec(f"db-mysql {new_source} -e 'select * from heartbeat.heartbeat where file like \"%{new_source}%\";'", dry_run)
        local_exec(f"db-mysql {new_source} -e 'delete from heartbeat.heartbeat where file like \"%{new_source}%\";'", dry_run)

        # Change events for query killer
        local_exec(f"curl -sS 'https://gerrit.wikimedia.org/r/plugins/gitiles/operations/software/+/refs/heads/master/dbtools/events_coredb_master.sql?format=TEXT' | base64 -d | sudo db-mysql {new_source}", dry_run)
        local_exec(f"curl -sS 'https://gerrit.wikimedia.org/r/plugins/gitiles/operations/software/+/refs/heads/master/dbtools/events_coredb_slave.sql?format=TEXT' | base64 -d | sudo db-mysql {old_source}", dry_run)

        logger.info("Failover completed successfully.")
    else:
        logger.info("Failover aborted by the user.")

    # Enable puppet
    remote_exec(new_source, 'puppet agent --enable', dry_run)
    remote_exec(old_source, 'puppet agent --enable', dry_run)

if __name__ == "__main__":
    main()
