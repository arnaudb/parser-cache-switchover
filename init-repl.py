#!/usr/bin/env python3

"""
Initialize MariaDB replication between two servers.

This script performs the following actions:

1. Connects to the source server and retrieves its binary log file and position.
2. Connects to the replica server and configures it for replication.
3. Starts the replication process on the replica server.
4. Verifies the replication status to ensure it is working correctly.

Example usage:

    python init-repl.py --source <source_server> --replica <replica_server>

Note: This script assumes that the replication credentials are stored in a YAML file at /etc/spicerack/cookbooks/sre.switchdc.databases.yaml.
"""

import argparse
import yaml
from helpers.runner import run_command

# Define the argument parser
parser = argparse.ArgumentParser(description='Initialize MariaDB replication')
parser.add_argument('--source', required=True, help='Source server')
parser.add_argument('--replica', required=True, help='Replica server')

# Parse the arguments
args = parser.parse_args()

# Load the replication credentials from the YAML file
with open('/etc/spicerack/cookbooks/sre.switchdc.databases.yaml', 'r') as f:
    config = yaml.safe_load(f)
repl_user = config['repl_user']
repl_pass = config['repl_pass']

# Define the source and replica server configurations
source_config = {
    'host': args.source,
    'port': 3306,
    'user': repl_user,
    'password': repl_pass
}

replica_config = {
    'host': args.replica,
    'port': 3306,
    'user': repl_user,
    'password': repl_pass
}

# Get the source server's binary log file and position
source_cmd = f"db-mysql {args.source} -e 'SHOW MASTER STATUS'"
source_output = run_command(source_cmd)
source_status = source_output.split('\n')[1].split('\t')
binlog_file = source_status[0]
binlog_position = source_status[1]

# Configure the replica server for replication
replica_cmd = f"db-mysql {args.replica} -e 'STOP SLAVE; CHANGE MASTER TO MASTER_HOST=\"{args.source}\", MASTER_PORT=3306, MASTER_USER=\"{repl_user}\", MASTER_PASSWORD=\"{repl_pass}\", MASTER_LOG_FILE=\"{binlog_file}\", MASTER_LOG_POS={binlog_position}; START SLAVE'"
replica_output = run_command(replica_cmd)

# Verify the replication status
replica_cmd = f"db-mysql {args.replica} -e 'SHOW SLAVE STATUS'"
replica_output = run_command(replica_cmd)
replica_status = replica_output.split('\n')[1].split('\t')
if replica_status[10] == 'Yes' and replica_status[11] == 'Yes':
    print("Replication initialized successfully")
else:
    print("Error initializing replication")
